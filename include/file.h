#ifndef __FILE_H__
#define __FILE_H__

#include <sys/types.h>
#include <dirent.h>

#define PATH_MAX        256

void list_dir (const char * dir_name);

#endif // __FILE_H__
