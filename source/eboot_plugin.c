#include <stdio.h>
#include <stdlib.h>

#include <kernel.h>
#include <systemservice.h>
#include <logdebug.h>
#include <orbis2d.h>          // ATTR_WIDTH, ATTR_HEIGHT
#include <orbisPad.h>
//#include <orbisXbmFont.h>


#include "egl.h"
#include "file.h"


// TIMING
#include <time.h>
static clock_t startm, stopm;
static uint16_t tick = 0;
static double fps = 0;

// for moving cursor
int x = ATTR_WIDTH  /2;
int y = ATTR_HEIGHT /2;
int w = ATTR_WIDTH /64;
int h = ATTR_WIDTH /64;
int step = 10;

// for liborbis2d
int64_t flipArg = 0;
int R,G,B;
uint32_t color = 0x80ff0000;
int flag = 0;

//Orbis2dConfig  *conf    = NULL;
OrbisPadConfig *confPad = NULL;


void updateController()
{
    int ret;
    unsigned int buttons=0;
    ret=orbisPadUpdate();
    if(ret==0)
    {
        if(orbisPadGetButtonPressed(ORBISPAD_L2|ORBISPAD_R2) || orbisPadGetButtonHold(ORBISPAD_L2|ORBISPAD_R2))
        {
            sys_log("Combo L2R2 pressed\n");
            buttons=orbisPadGetCurrentButtonsPressed();
            buttons&= ~(ORBISPAD_L2|ORBISPAD_R2);
            orbisPadSetCurrentButtonsPressed(buttons);
        }
        if(orbisPadGetButtonPressed(ORBISPAD_L1|ORBISPAD_R1) )
        {
            sys_log("Combo L1R1 pressed\n");
            buttons=orbisPadGetCurrentButtonsPressed();
            buttons&= ~(ORBISPAD_L1|ORBISPAD_R1);
            orbisPadSetCurrentButtonsPressed(buttons);
        }
        if(orbisPadGetButtonPressed(ORBISPAD_L1|ORBISPAD_R2) || orbisPadGetButtonHold(ORBISPAD_L1|ORBISPAD_R2))
        {
            sys_log("Combo L1R2 pressed\n");
            buttons=orbisPadGetCurrentButtonsPressed();
            buttons&= ~(ORBISPAD_L1|ORBISPAD_R2);
            orbisPadSetCurrentButtonsPressed(buttons);
        }
        if(orbisPadGetButtonPressed(ORBISPAD_L2|ORBISPAD_R1) || orbisPadGetButtonHold(ORBISPAD_L2|ORBISPAD_R1) )
        {
            sys_log("Combo L2R1 pressed\n");
            buttons=orbisPadGetCurrentButtonsPressed();
            buttons&= ~(ORBISPAD_L2|ORBISPAD_R1);
            orbisPadSetCurrentButtonsPressed(buttons);
        }
        if(orbisPadGetButtonPressed(ORBISPAD_UP) || orbisPadGetButtonHold(ORBISPAD_UP))
        {
            sys_log("Up pressed\n");

            if(y-step>=0)
            {
                y=y-step;
            }
            else
            {
                y=0;
            }
        }
        if(orbisPadGetButtonPressed(ORBISPAD_DOWN) || orbisPadGetButtonHold(ORBISPAD_DOWN))
        {
            sys_log("Down pressed\n");

            /*if(y+step<conf->height-1)
            {
                y=y+step;
            }
            else
            {
                y=conf->height-1-step;
            }*/
        }
        if(orbisPadGetButtonPressed(ORBISPAD_RIGHT) || orbisPadGetButtonHold(ORBISPAD_RIGHT))
        {
            sys_log("Right pressed\n");

            /*if(x+step<conf->width-1)
            {
                x=x+step;
            }
            else
            {
                x=conf->width-1-step;
            }*/
        }
        if(orbisPadGetButtonPressed(ORBISPAD_LEFT) || orbisPadGetButtonHold(ORBISPAD_LEFT))
        {
            sys_log("Left pressed\n");

            if(x-step>=0)
            {
                x=x-step;
            }
            else
            {
                x=0;
            }
        }
        if(orbisPadGetButtonPressed(ORBISPAD_TRIANGLE))
        {
            sys_log("Triangle pressed exit\n");

            flag=0;

        }
        if(orbisPadGetButtonPressed(ORBISPAD_CIRCLE))
        {
            sys_log("Circle pressed reset position and color red\n");
            x = ATTR_WIDTH  /2;
            y = ATTR_HEIGHT /2;
            color=0x80ff0000;
            //orbisAudioResume(0);

        }
        if(orbisPadGetButtonPressed(ORBISPAD_CROSS))
        {
            sys_log("Cross pressed rand color\n");
            R = rand() %256;
            G = rand() %256;
            B = rand() %256;
            color = 0x80000000|R<<16|G<<8|B;
            //orbisAudioStop();
            
            list_dir("/data");
        }
        if(orbisPadGetButtonPressed(ORBISPAD_SQUARE))
        {
            sys_log("Square pressed\n");
            //orbisAudioPause(0);
            main2();
        }
        if(orbisPadGetButtonPressed(ORBISPAD_L1))
        {
            sys_log("L1 pressed\n");

        }
        if(orbisPadGetButtonPressed(ORBISPAD_L2))
        {
            sys_log("L2 pressed\n");

        }
        if(orbisPadGetButtonPressed(ORBISPAD_R1))
        {
            sys_log("R1 pressed\n");

        }
        if(orbisPadGetButtonPressed(ORBISPAD_R2))
        {
            sys_log("R2 pressed\n");

        }
    }
}

void finishApp()
{
    //orbisAudioFinish();
    orbisPadFinish();
    //orbis2dFinish();
    //ps4LinkFinish();
}

void initApp()
{
    int ret;
    /*
    ret=ps4LinkInit("192.168.1.3",0x4711,0x4712,0x4712,DEBUG);
    if(!ret)
    {
        ps4LinkFinish();
        return;
    }
    while(!ps4LinkRequestsIsConnected())
    {

    }
    debugNetPrintf(DEBUG,"[PS4LINK] Initialized and connected from pc/mac ready to receive commands\n");
    */
    sys_log("App started\n");

    //hide playroom splash
    sceSystemServiceHideSplashScreen();

    ret = orbisPadInit();
    if(ret == 1)
    {
        confPad = orbisPadGetConf();

        ret = 1;//orbis2dInit();
        if(ret == 1)
        {
            //conf = orbis2dGetConf();
            flag = 1;
            /*
            ret=orbisAudioInit();
            if(ret==1)
            {
                ret=orbisAudioInitChannel(ORBISAUDIO_CHANNEL_MAIN,1024,48000,ORBISAUDIO_FORMAT_S16_STEREO);
            }
            */
        }
    }
}



int main(uint64_t stackbase, uint64_t othervalue) 
{
    initApp();
    
    

    while(flag)
    {
        //capture pad data and populate positions
        // X random color
        // O reset to center position and red color
        // /\ to exit
        // dpad move rectangle
        updateController();

        //wait for current display buffer
        //orbis2dStartDrawing();

        // clear with background (default white) to the current display buffer
        //orbis2dClearBuffer();

        // draw twister
        //draw_Twister();

        // default red is here press X to random color
        //orbis2dDrawRectColor(x,w,y,h,color);

        // draw text with Xbm_Font
        //sprintf(tmp_ln, "A Raster Twister Demo is drawn here");
        //print_text(tx, 400, tmp_ln);


//sys_log("%f FPS: drawn %d frames in %.3f sec\n", fps, tick, (double)(stopm-startm) /CLOCKS_PER_SEC);
if (1)  /* get timing, eventually compute fps */
{
        stopm = clock();
        if(((double)stopm-startm) > (3500000 / CLOCKS_PER_SEC))
        {
            fps = (double)(tick*1000000 / (((double)stopm-startm) * CLOCKS_PER_SEC));

            sys_log("%f FPS: drawn %d frames in %.3f sec\n", fps, tick, (double)(stopm-startm) * CLOCKS_PER_SEC);
            startm = clock(), tick = 0;              // reset fps counters      
        }
        tick++;
    
        // report fps, on the lowerleft corner
        //sprintf(tmp_ln, "%2.3fFPS", fps);
        //uint16_t x = get_aligned_x(tmp_ln, RIGHT);
        //print_text(x, ATTR_HEIGHT - (FONT_H + SHADOW_PX), tmp_ln);            
}  

        //flush and flip
        //orbis2dFinishDrawing(flipArg);

        //swap buffers
        //orbis2dSwapBuffers();
        //flipArg++;
    }

    /*
    orbisAudioResume(0);
    Mod_End();
    */
    finishApp();

    exit(0);

    return 0;
}
