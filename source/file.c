#include <stdio.h>

#include <string.h>
#include <logdebug.h>
#define    printf  sys_log

#include "file.h"

void list_dir (const char * dir_name)
{
    DIR * d;

    /* Open the directory specified by "dir_name". */

    d = opendir (dir_name);

    /* Check it was opened. */
    if (! d) {
        printf ("Cannot open directory '%s'\n",
                 dir_name);
        return ;
    }
    while (1) {
        struct dirent * entry;
        const char * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = readdir (d);
        if (! entry) {
            /* There are no more entries in this directory, so break
               out of the while loop. */
            break;
        }
        d_name = entry->d_name;
        /* Print the name of the file and directory. */
    printf ("%s/%s\n", dir_name, d_name);

#if 1
    /* If you don't want to print the directories, use the
       following line: */

        if (! (entry->d_type & DT_DIR)) {
        printf ("not a DIR:\t %s/%s\n", dir_name, d_name);
    }

#endif /* 0 */


        if (entry->d_type & DT_DIR) {

            /* Check that the directory is not "d" or d's parent. */
            
            if (strcmp (d_name, "..") != 0 &&
                strcmp (d_name, ".") != 0) {
                int path_length;
                char path[PATH_MAX];
 
                path_length = snprintf (path, PATH_MAX,
                                        "%s/%s", dir_name, d_name);
                printf ("DIR path: %s\n", path);
                if (path_length >= PATH_MAX) {
                    printf ("Path length has got too long.\n");
                    return ;
                }
                /* Recursively call "list_dir" with the new path. */
                //list_dir (path);
            }
    }
    }
    /* After going through all the entries, close the directory. */
    if (closedir (d)) {
        printf ("Could not close '%s'\n",
                 dir_name);
        return ;
    }
}
