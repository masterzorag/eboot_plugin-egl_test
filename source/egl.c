#include <stdio.h>
#include <stdlib.h>

//#define GLFW_INCLUDE_ES2
//#include <GLFW/glfw3.h>

//
// clang egl.c -lGL -lEGL -ggdb
//

#ifdef __PS4__
 #include <logdebug.h>
 #define  printf  sys_log
 #include <GLES/egl.h>
 #include <GLES/gl.h>

#else
 #include <EGL/egl.h>
 #include <GLES2/gl2.h>

#endif

static const GLuint WIDTH = 1280;
static const GLuint HEIGHT = 720;
static const GLchar* vertex_shader_source =
    "#version 100\n"
    "attribute vec3 position;\n"
    "void main() {\n"
    "   gl_Position = vec4(position, 1.0);\n"
    "}\n";
static const GLchar* fragment_shader_source =
    "#version 100\n"
    "void main() {\n"
    "   gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n"
    "}\n";
static const GLfloat vertices[] = {
        0.0f,  0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
    -0.5f, -0.5f, 0.0f,
};

GLint common_get_shader_program(const char *vertex_shader_source, const char *fragment_shader_source) {
    enum Consts {INFOLOG_LEN = 512};
    GLchar infoLog[INFOLOG_LEN];
    GLint fragment_shader;
    GLint shader_program;
    GLint success;
    GLint vertex_shader;

    /* Vertex shader */
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_shader, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    }

    /* Fragment shader */
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragment_shader, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    }

    /* Link shaders */
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shader_program, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    return shader_program;
}

#ifdef __PS4__
int main2(void)
#else
int main(void)
#endif
{
    GLuint shader_program, vbo;
    GLint pos;

    EGLDisplay eglDisplay = NULL;
    printf("\nEGL test-start, calling eglDisplay\n");
    eglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    printf("eglDisplay\t: %p\n", eglDisplay);

    // Initialize EGL for this display, returns EGL version
    EGLint eglVersionMajor, eglVersionMinor;
    int ret = 1337;

    ret = eglInitialize(eglDisplay, &eglVersionMajor, &eglVersionMinor);
    printf("eglInitialize\t: %d\n", ret);    
    
    printf("eglVersionMajor\t: %d\n", eglVersionMajor);
    printf("eglVersionMinor\t: %d\n", eglVersionMinor);
    
    ret = eglBindAPI(EGL_OPENGL_ES_API);
    printf("eglBindAPI\t: %d\n", ret);  
    
    EGLint configAttributes[] =
    {
    	EGL_BUFFER_SIZE, 0,
    	EGL_RED_SIZE, 5,
    	EGL_GREEN_SIZE, 6,
    	EGL_BLUE_SIZE, 5,
    	EGL_ALPHA_SIZE, 0,
    	EGL_COLOR_BUFFER_TYPE, EGL_RGB_BUFFER,
    	EGL_DEPTH_SIZE, 24,
    	EGL_LEVEL, 0,
    	EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
    	EGL_SAMPLE_BUFFERS, 0,
    	EGL_SAMPLES, 0,
    	EGL_STENCIL_SIZE, 0,
    	EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
    	EGL_TRANSPARENT_TYPE, EGL_NONE,
    	EGL_TRANSPARENT_RED_VALUE, EGL_DONT_CARE,
    	EGL_TRANSPARENT_GREEN_VALUE, EGL_DONT_CARE,
    	EGL_TRANSPARENT_BLUE_VALUE, EGL_DONT_CARE,
    	EGL_CONFIG_CAVEAT, EGL_DONT_CARE,
    	EGL_CONFIG_ID, EGL_DONT_CARE,
    	EGL_MAX_SWAP_INTERVAL, EGL_DONT_CARE,
    	EGL_MIN_SWAP_INTERVAL, EGL_DONT_CARE,
    	EGL_NATIVE_RENDERABLE, EGL_DONT_CARE,
    	EGL_NATIVE_VISUAL_TYPE, EGL_DONT_CARE,
    	EGL_NONE
    };
    
    //// another minimal set of attributes
    EGLint attr[] = {       // some attributes to set up our egl-interface
      EGL_BUFFER_SIZE, 16,
      EGL_RENDERABLE_TYPE,
      EGL_OPENGL_ES2_BIT,
      EGL_NONE
   };
    
    EGLConfig ecfg;
    EGLint    numConfigs;
    
    if(!eglChooseConfig(eglDisplay, configAttributes, &ecfg, 1, &numConfigs)) {
        printf("Failed to choose config (eglError: 0x%x)\n", eglGetError()); // return 1
    }
    if (numConfigs != 1 ) {
        printf("Didn't get exactly one config, but %d\n", numConfigs); // return 1
    }
    printf("eglChooseConfig\t: %p\n", eglChooseConfig);    
    
    //EGLint surfaceAttributes[] = { EGL_NONE };
    EGLSurface eglSurface = eglCreateWindowSurface(eglDisplay, ecfg, 0, 0);
    if ( eglSurface == EGL_NO_SURFACE ) {
        printf("Unable to create EGL surface (eglError: 0x%x)\n", eglGetError()); // return 1
    }  
    printf("eglSurface\t: %p\n", eglSurface); 
       
    //// egl-contexts collect all state descriptions needed required for operation 
    EGLint contextAttributes[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE };
    EGLContext eglContext = eglCreateContext(eglDisplay, ecfg, EGL_NO_CONTEXT, contextAttributes);
    if ( eglContext == EGL_NO_CONTEXT ) {
        printf("Unable to create EGL context (eglError: 0x%x)", eglGetError()); //  return 1;
    }
    printf("eglContext\t: %p\n", eglContext);


//// optional, query context
EGLint value = 1337;
printf("eglQueryContext\n");
eglQueryContext(eglDisplay, eglContext, EGL_CONTEXT_CLIENT_TYPE, &ret);
printf(" EGL_CONTEXT_CLIENT_TYPE : %.x\n", ret);
eglQueryContext(eglDisplay, eglContext, EGL_RENDER_BUFFER, &ret);
printf(" EGL_RENDER_BUFFER       : %.x\n", ret);


//eglQuerySurface(mDisplay, mSurface, EGL_RENDER_BUFFER, &renderBuffer)

    //// associate the egl-context with the egl-surface
    ret = eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
    printf("eglMakeCurrent\t: %.x\n", ret); 

    printf("GL_VERSION \t: %s\n", glGetString(GL_VERSION) );
    printf("GL_RENDERER\t: %s\n", glGetString(GL_RENDERER) );  
    
	/*GLFWwindow* window;

    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    window = glfwCreateWindow(WIDTH, HEIGHT, __FILE__, NULL, NULL);
	glfwMakeContextCurrent(window);
	*/
//sleep(2);
    
    glClearColor(0.0f, 0.0f, 5.0f, 1.0f);
    glViewport(0, 0, WIDTH, HEIGHT);
    
    
    shader_program = common_get_shader_program(vertex_shader_source, fragment_shader_source);
    pos = glGetAttribLocation(shader_program, "position");

    //return 0; 

    

ret = eglTerminate(eglDisplay);
    printf("eglTerminate\t: %d\n\n\n", ret);    

    return 0;    
    
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(pos);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    while (1) {
        glClear(GL_COLOR_BUFFER_BIT);
        glUseProgram(shader_program);
        glDrawArrays(GL_TRIANGLES, 0, 3);
		//        glfwSwapBuffers(window);
 		eglSwapBuffers(eglDisplay, eglSurface);
     }
    glDeleteBuffers(1, &vbo);
    //glfwTerminate();
    return EXIT_SUCCESS;
}